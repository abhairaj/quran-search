import { Component } from '@angular/core';

//this is a typescript hack - should be using http get
declare var require: any;
const packageInfo = require('../../package.json');

@Component({
    moduleId: module.id,
    selector: 'about',
    templateUrl: './about.html',
})
export class AboutView {
    private packageInfo;

    constructor() {
        this.packageInfo = packageInfo;
    }
}

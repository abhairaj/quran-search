import { Component } from '@angular/core';
import { SearchService } from './search.service';

@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: './app.html',
  styleUrls: ['./app.css'],
  providers: [SearchService]
})
export class AppComponent { name = 'Quran Search'; }

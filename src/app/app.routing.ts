import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutView } from './about.view';
import { BrowseView } from './browse.view';
import { SearchView } from './search.view';

const routes: Routes = [
    { path: '', component: SearchView }
    , { path: 'about', component: AboutView }
    , { path: 'browse', component: BrowseView }
    , { path: 'browse/:chapterId/:translationId', component: BrowseView }
    , { path: 'search', component: SearchView }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
import { Component, Input, OnChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.css']
})
export class PagerComponent implements OnChanges {
  @Input()
  itemsSize: number; //the total number of items to page over
  @Input()
  currentItem: number;
  @Input()
  pageSize: number; //the number pf items per page
  @Input()
  small: boolean = false; //whether or not to use bootstrap pagination-sm class
  @Input()
  max: number = 22; // the max number of pages to show (not including first/last, previous/next)
  @Input()
  showFirstLast = true;
  @Input()
  showPreviousNext = true;

  pagesSize: number;
  currentPage: number;
  pages: number[];

  @Output()
  onPaginate: EventEmitter<number> = new EventEmitter();

  ngOnChanges() {
    this.pagesSize = Math.floor(this.itemsSize / this.pageSize);
    this.currentPage = Math.ceil((this.currentItem) / this.pageSize)
    let half = Math.floor(this.max / 2);
    let startPage: number;
    let endPage: number;

    if (this.pagesSize > this.max) {
      if (this.currentPage <= half) {
        startPage = 1;
        endPage = this.max;
      } else if (this.currentPage >= this.pagesSize - half) {
        startPage = this.pagesSize - this.max;
        endPage = this.pagesSize;
      } else {
        startPage = this.currentPage - half;
        endPage = this.currentPage + half;
      }

    } else {
      startPage = 1;
      endPage = this.pagesSize;
    }

    this.pages = [];
    for (var i = startPage - 1; i <= endPage; i++) {
      this.pages.push(i);
    }
  }

  paginate(to: number) {
    this.onPaginate.emit(to);
  }

}

export class ResponseHeader {
    status: number;
    params: any;
};

export class FacetContainer {
    constructor(show: boolean) {
        this.show = show;
    }
    useable: number = 0;
    show: boolean = true;
    facets: Facet[] = [];
};

export class Facet {
    value: string;
    count: number;
    active: boolean = false;
};
import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { environment } from './../environments/environment';
import 'rxjs/add/operator/map';

const SERVICE_SEGMENT: string = 'browseChapter';
const ROWS: number = 100;

@Injectable()
export class BrowseService {
    private baseUrl = environment.baseUrl;
    public chapterId: number = 1;

    constructor(private _http: Http) { }

    public browse(chapterId: number) {
        this.chapterId = chapterId;
        var params: URLSearchParams = new URLSearchParams();
        params.set('wt', 'json');
        params.set('indent', 'on');
        params.set('q', 'chapterId:' + this.chapterId);
        params.set('rows', ROWS.toString());
        return this._http.get(this.baseUrl + SERVICE_SEGMENT, { search: params }).map(res => res.json());
    }
}


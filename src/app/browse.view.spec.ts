/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { BrowseView } from './browse.view';

describe('BrowseView', () => {
  let component: BrowseView;
  let fixture: ComponentFixture<BrowseView>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrowseView ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrowseView);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

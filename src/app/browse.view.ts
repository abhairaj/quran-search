import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { BrowseService } from './browse.service';
import { FacetContainer, Facet, ResponseHeader } from './model';

//this is a typescript hack - should be using http get
declare var require: any;
const TRANSLATIONS = require('../assets/translations.json');

@Component({
  selector: 'browse',
  templateUrl: './browse.html'
})
export class BrowseView implements OnInit {
  private chapterId: number;
  private translationId: number = 1;
  private doc: any;
  private original: string[];
  private transliteration: string[];
  private header: ResponseHeader;
  private translations: FacetContainer = new FacetContainer(true);
  private response: any;
  private MAX_CHAPTERS: number = 114;
  private working: boolean = false;

  constructor(public service: BrowseService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.browse(+params['chapterId'] ? +params['chapterId'] : this.service.chapterId, +params['translationId'] ? +params['translationId'] : this.translationId);
    });
  }

  browse(chapterId: number, translationId: number) {
    this.working = true;
    this.chapterId = chapterId;
    this.translationId = translationId;
    this.service.browse(this.chapterId).subscribe(res => { this._applyResponse(res); this.working = false; });
  }

  changeDoc(translationId: number) {
    this.translationId = translationId;
    this.doc = this.response.docs.filter(this.docTranslationFilter(this.translationId))[0];
    for (var i = 0; i < this.translations.facets.length; i++) {
      var f = this.translations.facets[i];
      f.active = f.value == this.translationId.toString();
    }
  }

  translationRef(id: number) {
    var result = TRANSLATIONS.filter(function (el) {
      return el.id == id;
    });
    return result.length > 0 ? result[0] : { 'name': 'unknown' };
  }

  private _applyResponse(res: any) {
    this.header = res.responseHeader;
    this.response = res.response;
    this.doc = this.response.docs.filter(this.docTranslationFilter(this.translationId))[0];
    //console.log(this.doc.text[0]);

    this.original = this.response.docs.filter(this.docTranslationFilter(1))[0].text[0].split('\n');
    this.transliteration = this.response.docs.filter(this.docTranslationFilter(63))[0].text[0].split('\n');
    this.translations = this.facets(res.facet_counts.facet_fields.translationId, this.translations.show, this.translationId, [1, 63]);
    this.translations.facets.sort(function (a: Facet, b: Facet) {
      var nameA = TRANSLATIONS.filter(function (T) {
        return T.id == a.value;
      })[0].name;
      var nameB = TRANSLATIONS.filter(function (T) {
        return T.id == b.value;
      })[0].name;
      return nameA < nameB ? -1 : nameA > nameB ? 1 : 0;
    });
  }

  private docTranslationFilter(translationId: number) {
    return function (doc) {
      return doc.translationId == translationId;
    }
  }

  private facets(fields: any[], show: boolean, activeValue: any, excludeValues: any[]) {
    var fc = new FacetContainer(show);
    for (var i = 0; i < fields.length / 2; i++) {
      var f = new Facet();
      f.value = fields[i * 2];
      f.count = fields[(i * 2) + 1];
      f.active = f.value == activeValue
      if (excludeValues.indexOf(f.value) == -1) {
        fc.facets.push(f);
      }
    }
    return fc;
  }

}

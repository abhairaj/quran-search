import { Component, OnInit } from '@angular/core';
import { SearchService } from './search.service';
import { FacetContainer, Facet, ResponseHeader } from './model';
import 'rxjs/add/operator/map';

//this is a typescript hack - should be using http get
declare var require: any;
const AUTHORS = require('../assets/authors.json');
const CHAPTERS = require('../assets/chapters.json');
const LANGS = require('../assets/langs.json');

@Component({
    moduleId: module.id,
    selector: 'search',
    templateUrl: 'search.html',
})
export class SearchView implements OnInit {
    private authors: FacetContainer = new FacetContainer(true);
    private chapters: FacetContainer = new FacetContainer(true);
    private header: ResponseHeader;
    private hl: any[];
    private languages: FacetContainer = new FacetContainer(true);
    private q: string;
    private response: any;
    private _authorRef;
    private _chapterRef;
    private _langRef;
    private working: boolean = false;

    constructor(private _service: SearchService) { }

    ngOnInit() {
        this._authorRef = AUTHORS;
        this._chapterRef = CHAPTERS;
        this._langRef = LANGS;
        this.q = this._service.q;
        if (this.q != undefined) {
            this._service.search(this.q).subscribe(res => {
                this._applyResponse(res);
            });
        }
    }

    authorRef(code: string) {
        var result = this._authorRef.authors.filter(function (el) {
            return el.code == code;
        });
        return result.length > 0 ? result[0] : { 'name': 'unknown' };
    }

    chapterRef(chapterId: number, iso2: string) {
        var result = this._chapterRef.chapters[chapterId - 1].langs.filter(function (el) {
            return el.iso2 == iso2;
        });
        return result.length > 0 ? result[0] : { 'name': 'unknown' };
    }

    langRef(iso2: string) {
        var result = this._langRef.langs.filter(function (el) {
            return el.iso2 == iso2;
        });
        return result.length > 0 ? result[0] : { 'name': 'unknown' };
    }

    paginate(page: number) {
        this.working = true;
        this._service.paginate(page).subscribe(res => {
            this._applyResponse(res);
            this.working = false;
        });
    }

    search() {
        this.working = true;
        this._service.search(this.q).subscribe(res => {
            this._applyResponse(res);
            this.working = false;
        });
    }

    toggleFilter(facet: string, value: string) {
        this.working = true;
        this._service.toggleFilter(facet, value).subscribe(res => {
            this._applyResponse(res);
            this.working = false;
        });
    }

    private _applyResponse(res: any) {
        this.header = res.responseHeader;
        this.response = res.response;
        for (var i = 0; i < this.response.docs.length; i++) {
            this.response.docs[i].show = false;
        }
        this.authors = this.facets('authorCode', res.facet_counts.facet_fields.authorCode, this.authors.show);
        this.chapters = this.facets('chapterId', res.facet_counts.facet_fields.chapterId, this.chapters.show);
        this.languages = this.facets('langCode', res.facet_counts.facet_fields.langCode, this.languages.show);
        this.hl = res.highlighting;
    }

    private facets(fieldName: string, fields: any[], show: boolean) {
        var fc = new FacetContainer(show);
        for (var i = 0; i < fields.length / 2; i++) {
            var f = new Facet();
            f.value = fields[i * 2];
            f.count = fields[(i * 2) + 1];
            f.active = this._isFiltered(fieldName + ':' + f.value);
            if (f.count > 0) { fc.useable++ }
            fc.facets.push(f);
        }
        return fc;
    }

    private _isFiltered(fq: string) {
        return this.header.params == undefined ? false
            : this.header.params.fq == undefined ? false
                : Array.isArray(this.header.params.fq) ? this.header.params.fq.includes(fq)
                    : this.header.params.fq == fq;
    }
}

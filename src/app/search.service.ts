import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { environment } from './../environments/environment';
import 'rxjs/add/operator/map';

const SERVICE_SEGMENT: string = 'select';

@Injectable()
export class SearchService {
    private baseUrl = environment.baseUrl;
    pageSize: number = 54;
    private authors: string[] = [];
    private chapters: string[] = [];
    private languages: string[] = [];
    private start: number = 0;
    public q: string;

    constructor(private _http: Http) { }

    paginate(start: number) {
        this.start = start;
        return this.get();
    }

    search(q: string) {
        this.start = 0;
        this.q = q;
        return this.get();
    }

    toggleFilter(facet: string, value: string) {
        this.start = 0;
        switch (facet) {
            case 'author':
                var index = this.authors.indexOf(value);
                index < 0 ? this.authors.push(value) : this.authors.splice(index);
                break;
            case 'language':
                var index = this.languages.indexOf(value);
                index < 0 ? this.languages.push(value) : this.languages.splice(index);
                break;
            case 'chapter':
                var index = this.chapters.indexOf(value);
                index < 0 ? this.chapters.push(value) : this.chapters.splice(index);
        }
        return this.get();
    }

    private get() {
        var params: URLSearchParams = new URLSearchParams();
        params.set('wt', 'json');
        params.set('indent', 'on');
        params.set('q', this.q == undefined || this.q.trim().length == 0 ? '*:*' : 'text:' + this.q);
        params.set('start', this.start.toString());
        params.set('rows', this.pageSize.toString());
        for (var i = 0; i < this.authors.length; i++) {
            params.append('fq', 'authorCode:' + this.authors[i]);
        }
        for (var i = 0; i < this.chapters.length; i++) {
            params.append('fq', 'chapterId:' + this.chapters[i]);
        }
        for (var i = 0; i < this.languages.length; i++) {
            params.append('fq', 'langCode:' + this.languages[i]);
        }
        return this._http.get(this.baseUrl + SERVICE_SEGMENT, { search: params }).map(res => res.json());
    }
}


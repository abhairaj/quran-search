import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SpinnerModule } from 'angular2-spinner/dist';

import { AppComponent } from './app.component';
import { SearchView } from './search.view';
import { SearchService } from './search.service';
import { BrowseService } from './browse.service';
import { AboutView } from './about.view';
import { BrowseView } from './browse.view';
import { routing } from './app.routing';
import { PagerComponent } from './pager.component';

@NgModule({
  declarations: [AppComponent, SearchView, AboutView, BrowseView, PagerComponent],
  imports: [BrowserModule, FormsModule, HttpModule, routing, SpinnerModule],
  providers: [BrowseService, SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
